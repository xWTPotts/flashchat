//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Taylor Potts on 2020-08-18.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import Foundation

struct Message {
    let sender: String // The email address of the person that sent th message
    let body: String // The text itself
}
